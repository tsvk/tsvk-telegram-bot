const token = "%Telegram Bot Token%";
const telegramUrl = "https://api.telegram.org/bot" + token;
const webAppUrl = "https://script.google.com/macros/s/%webAppId%/exec";
var ssId = "%Google Sheet ID%";
var ss = SpreadsheetApp.openById(ssId);
var sheet = ss.getSheets()[0];

var sub = [{
        id: 0,
        name: "dnb"
    },
    {
        id: 1,
        name: "house"
    },
    {
        id: 2,
        name: "techno"
    },
    {
        id: 3,
        name: "verbalius"
    },
    {
        id: 4,
        name: "satoshi"
    }
]

function getMe() {
    var url = telegramUrl + "/getMe";
    var res = UrlFetchApp.fetch(url);
    Logger.log(res.getContentText());
}

function setWebhook() {
    var url = telegramUrl + "/setWebhook?url=" + webAppUrl;
    var res = UrlFetchApp.fetch(url);
    Logger.log(res.getContentText());
}

function doPost(e) {
    var update = JSON.parse(e.postData.contents);
    if (update.message) {
        var msg = update.message;
        var chatId = msg.chat.id;
        var text = msg.text;

        if (text == '/start' || text == '/help') {
            checkIfUser(chatId);
        }

    } else if (update.callback_query) {
        var updatex = update.callback_query;
        var chatId = updatex.from.id;
        var action = updatex.data;
        var msgId = updatex.message.message_id;

        if (action.indexOf('add') != -1 || action.indexOf('rm') != -1) {

            var registered = checkIfRegistered(chatId);

            if (registered) {
                if (action.indexOf('add') != -1) {
                    var row = sheet.getRange("B" + registered);
                    row.setValue(row.getValue() + action.split('_')[1] + ';');
                } else {
                    var row = sheet.getRange("B" + registered);
                    var values = row.getValue().split(';');
                    var newValue = '';
                    for (var i in values) {
                        if (values[i] != action.split('_')[1] && i != values.length-1) {
                            newValue += values[i] + ';';
                        }
                    }

                    row.setValue(newValue);
                }
            } else {
                if (action.indexOf('add') != -1) {
                    registerUser(chatId, action.split('_')[1]);
                }
            }
            var inline = updatex.message.reply_markup.inline_keyboard;

            for (var i in inline) {
                for (var j in inline[i]) {
                    if (inline[i][j].callback_data == action && action.indexOf('add') !== -1) {
                        inline[i][j] = {
                            text: '\u{2705} ' + inline[i][j].text,
                            callback_data: "rm_" + inline[i][j].callback_data.split('_')[1]
                        };
                    } else if (inline[i][j].callback_data == action && action.indexOf('rm') !== -1) {
                        inline[i][j] = {
                            text: inline[i][j].text.substr(1),
                            callback_data: "add_" + inline[i][j].callback_data.split('_')[1]
                        };
                    }
                }
            }

            var options = JSON.stringify({
                inline_keyboard: inline
            });
            UrlFetchApp.fetch(telegramUrl + "/editMessageReplyMarkup?chat_id=" + chatId + "&message_id=" + msgId + "&reply_markup=" + encodeURIComponent(options));
        }
    }
}

function checkIfUser(chatId) {
    var registered = checkIfRegistered(chatId);
    var textMsg = '',
        subs = sub,
        available = [],
        added = [];
    
    if (registered) {
        textMsg = "Твої поточні підписки";
        var value = sheet.getRange("B" + registered).getValue();
        
        if (value.length > 0) {
            added = value.split(';');
            for (var l in subs) {
                if (added.includes(String(subs[l].id))) {
                    subs[l].name = "\u{2705} " + subs[l].name;
                }
            }
        }
    } else {
        textMsg = "Привіт, обери тему які тебе цікавлять, і бот надішле сповіщення коли у ефірі радіо буде щось з цього";
    }    
        for (var j = 0; j < subs.length; j++) {
            if (subs[parseInt(j) + parseInt(2)] != undefined) {
                if (added.includes(String(subs[j].id))) {
                    var callback = 'rm_' + subs[j].id;
                } else {
                    var callback = 'add_' + subs[j].id;
                }
                if (added.includes(String(subs[parseInt(j) + parseInt(1)].id))) {
                    var callback1 = 'rm_' + subs[parseInt(j) + parseInt(1)].id;
                } else {
                    var callback1 = 'add_' + subs[parseInt(j) + parseInt(1)].id;
                }
                if (added.includes(String(subs[parseInt(j) + parseInt(2)].id))) {
                    var callback2 = 'rm_' + subs[parseInt(j) + parseInt(2)].id;
                } else {
                    var callback2 = 'add_' + subs[parseInt(j) + parseInt(2)].id;
                }
                available.push([{
                    text: subs[j].name,
                    callback_data: callback
                }, {
                    text: subs[parseInt(j) + parseInt(1)].name,
                    callback_data: callback1
                }, {
                    text: subs[parseInt(j) + parseInt(2)].name,
                    callback_data: callback2
                }]);
                j = j + 2;
            } else if (subs[parseInt(j) + parseInt(1)] != undefined) {
                if (added.includes(String(subs[j].id))) {
                    var callback = 'rm_' + subs[j].id;
                } else {
                    var callback = 'add_' + subs[j].id;
                }
                if (added.includes(String(subs[parseInt(j) + parseInt(1)].id))) {
                    var callback1 = 'rm_' + subs[parseInt(j) + parseInt(1)].id;
                } else {
                    var callback1 = 'add_' + subs[parseInt(j) + parseInt(1)].id;
                }
                available.push([{
                    text: subs[j].name,
                    callback_data: callback
                }, {
                    text: subs[parseInt(j) + parseInt(1)].name,
                    callback_data: callback1
                }]);
                j++;
            } else {
                if (added.includes(String(subs[j].id))) {
                    var callback = 'rm_' + subs[j].id;
                } else {
                    var callback = 'add_' + subs[j].id;
                }
                available.push([{
                    text: subs[j].name,
                    callback_data: callback
                }]);
            }
        }

        var options = JSON.stringify({
            inline_keyboard: available
        })
        return UrlFetchApp.fetch(telegramUrl + "/sendMessage?chat_id=" + chatId + "&text=" + textMsg + "&reply_markup=" + encodeURIComponent(options));


}

function checkIfRegistered(chatId) {
    for (var i = 1; i <= sheet.getLastRow(); i++) {
        var cell_val = String(sheet.getRange("A" + i).getDisplayValue());
        if (String(cell_val) == String(chatId)) return i;
    }
    return false;
}

function registerUser(chatId, id) {
    var lastRow = sheet.getLastRow();
    sheet.getRange(lastRow + 1, 1).setValue(chatId);
    sheet.getRange(lastRow + 1, 2).setValue(id + ';');
}

var notifiedUser = [];
function parseUrl() {
    try {
        var html = UrlFetchApp.fetch('https://tusovka.ml/whats_playin');
        var string1 = html.getContentText().split('"server_description":"')[1];
        var string2 = string1.split('","server_name')[0];
        var lastDescCell = sheet.getRange("D1");
        if (lastDescCell.getValue() != string2) {
            notifiedUser = [];
            lastDescCell.setValue(string2);
            for (var i in sub) {
                if (string2.toLowerCase().indexOf(sub[i].name) != -1) {
                    runNotify(sub[i].id, string2);
                }
            }
        }
    } catch (e) { }
}

function runNotify(id, desc) {
      for(var i = 1; i <= sheet.getLastRow(); i++) {      
        var userId = String(sheet.getRange("A"+i).getDisplayValue());
        if(!notifiedUser.includes(userId)){
            var userSubs = String(sheet.getRange("B" + i).getDisplayValue());
            if(userSubs.indexOf(id) != -1) {
                notifiedUser.push(userId);
                var pre = encodeURIComponent("В ефірі одна з твоїх підписок\n\nПрограма:\n");
                var options = JSON.stringify({
                    inline_keyboard: [[{ text: "Тусуватися \u{1F57A}", url: "https://tusovka.ml" }]]
                });
                UrlFetchApp.fetch(telegramUrl + "/sendMessage?chat_id=" + userId + "&text="+ pre + encodeURIComponent(desc) +"&reply_markup=" + encodeURIComponent(options));          
            }
        }
    }
}
